fn main() {
    // define both multiplicators
    let a: i64 = 239239239239;
    let b: i64 = 12121212;

    // create col lists
    let mut left_col = Vec::new();
    let mut right_col = Vec::new();

    // initiate variables for filling cols
    let mut a_copy = a;
    let mut two_exponents = 1;

    while two_exponents < b {
        left_col.push(a_copy);
        right_col.push(two_exponents);
        a_copy = a_copy + a_copy;
        two_exponents = two_exponents + two_exponents;
    }

    // reduce debug prints to one to avoid stdout overhead
    println!(
        "a: {} | b: {}\nleft col: {:?}\nright col: {:?}",
        a, b, left_col, right_col
    );

    // zip both lists to iterate over both cols simultaneously
    // We basically iterativly consume both lists at the same time in reversed order (`Iterator::rev`)
    // While iterating we also use `Iterator::fold` to keep both sum and result in the loop
    // This could easily be parallelised with sth like rayon parallel iterators but that would be overkill at this point
    let (_, result) =
        right_col
            .into_iter()
            .zip(left_col)
            .rev()
            .fold((0, 0), |(sum, result), (r, l)| {
                if sum + r <= b {
                    (sum + r, result + l)
                } else {
                    (sum, result)
                }
            });

    // second debug print
    // result will most likely be inlined but it's seperated for readability
    // We also use the new Format Strings from Rust 1.58!(
    println!(
        "{a} x {b} = {result}\ncheck:\n{a} x {b} = {check_result}",
        a = a,
        b = b,
        result = result,
        check_result = a * b
    );
}
