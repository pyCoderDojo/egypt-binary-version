fn main() {
    let a = 47;
    let b = 23;
    let mut b_copy = b;

    let mut binary_representation = Vec::new();

    while b_copy > 0 {
        binary_representation.push(b_copy % 2);
        b_copy /= 2;
    }

    println!(
        "Calculating for {} x {} = {}",
        a,
        b,
        binary_representation
            .into_iter()
            .fold((0, a), |acc, bit| {
                if bit == 1 {
                    (acc.0 + acc.1, acc.1 + acc.1)
                } else {
                    (acc.0, acc.1 + acc.1)
                }
            })
            .0
    );
}
