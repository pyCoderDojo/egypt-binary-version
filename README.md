# egypt-rs

## Compiling

- You need a working rust toolchain, preferably [rustup](https://rustup.rs/). With at least rust 1.58.0

Compile with `cargo build --release --bin` and get the executables from `target/release`.

Set the environment flag `RUSTFLAGS` to `"-C target-cpu=native"` for further compile-time optimizations.

> Note: This program is only semi-optimized. It's recommended to take a look at SIMD for max performance.
